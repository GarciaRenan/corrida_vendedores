import React from 'react';
import { useState, useEffect } from 'react';
import If from './If';

export default props => {

const ScreenSetter = props.ScreenSetter;
const screenList = props.screenList;

const [leaderboard, setLeaderboard] = useState(props.leaderboardData);
const [leagueLeaders, setLeagueLeaders] = useState([]);
const [stateLeaders, setStateLeaders] = useState([]);
const [boardIsSorted, setBoardIsSorted] =useState(false);

useEffect(() => {    
    let items = leaderboard.map(salesman => salesman);
    items.sort((a, b) => (parseFloat(a.valorTotal) < parseFloat(b.valorTotal)) ? 1 : (parseFloat(a.valorTotal) === parseFloat(b.valorTotal)) ? (parseInt(a.quantidadeTotal) < parseInt(b.quantidadeTotal)) ? 1 : -1 : -1);
    setLeaderboard(items);
    setBoardIsSorted(true);    
}, []);

useEffect(() => {
    console.log(leagueLeaders);
}, [leagueLeaders]);

useEffect(() => {
    if(boardIsSorted) {
        const leagues = ['a', 'b', 'c'];
        let leagueChampions = [];
        
        leagues.forEach(league => {
            let champ = leaderboard.find(element => element.grupo === league);
            if (champ) {
                leagueChampions.push(champ);
            }
        });

        setLeagueLeaders(leagueChampions);

        const states = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP' , 'SE', 'TO'];
        let stateChampions = [];

        states.forEach(state => {
            let champ = leaderboard.find(element => element.estado === state);
            if (champ) {
                stateChampions.push(champ);
            }
        });

        setStateLeaders(stateChampions);
    } 
}, [boardIsSorted, leaderboard]);


    return (        
        <div className="leaderboard">
            <img src={props.headliner} alt="Ranking dos Melhores" className="headline"/>
            <div className="division">
                <h5>Geral</h5>
                <div className="winners_list">
                    <div className="champion">
                        <legend>{leaderboard[0].nome}/{leaderboard[0].estado}</legend>
                        <span>1</span>
                    </div>  
                </div>                              
            </div>
            <div className="division">
                <h5>Por liga</h5>
                <div className="winners_list">
                    {leagueLeaders.map(leader => (
                        <div className="champion">
                            <legend>{leader.nome}/{leader.estado}</legend>
                            <span>{leader.grupo}</span>
                        </div> 
                    ))} 
                </div>                              
            </div>
            <div className="division">
                <h5>Por estado</h5>
                <div className="winners_list">
                {stateLeaders.map(leader => (
                        <div className="champion state_champ">
                            <legend>{leader.nome}/{leader.estado}</legend>
                            <span>{leader.estado}</span>
                        </div> 
                    ))} 
                </div>                              
            </div>
            <div className="nav_buttons">
                <button onClick={() => ScreenSetter(screenList.start)}>
                    Voltar
                </button>     
            </div>   
        </div>
    )    
}