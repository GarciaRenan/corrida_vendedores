import React, {useState, useEffect} from 'react';
import AutocompleteCity from './AutocompleteCity';
import InputMask from "react-input-mask";
import { Link } from "react-router-dom";
import If from './If';


const RegisterSeller = props => {

    const NotificationSetter = props.NotificationSetter;

    const [cityList, setCityList] = useState({loading: false, items: null });
    const [showCityList, setShowCityList] = useState(false);

    const [sellersInfo, setSellersInfo] = useState({loading: false, items: null });
    const [repeatedEmail, setRepeatedEmail] = useState(false);

    const [groupInfo, setGroupInfo] = useState({loading: false, items: null });
    const [group, setGroup] = useState('c');

    const [invalidCnpj, setInvalidCnpj] = useState(false);

    const [name, setName] = useState("");
    const [cpf, setCpf] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [cnpj, setCnpj] = useState("");
    const [uf, setUf] = useState("Estado");
    const [city, setCity] = useState("");

    useEffect(() => {
        setGroupInfo({ loading: true });
        fetch('https://api.sheety.co/0a34b7201289c69647d7f8c626404ba2/vendedoresCadastrados/grupos/')
        .then(response => response.json())
        .then(data => setGroupInfo({ loading: false, items: data.grupos}))
    }, []);

    useEffect(() => {
        setSellersInfo({ loading: true });
        fetch('https://api.sheety.co/0a34b7201289c69647d7f8c626404ba2/vendedoresCadastrados/vendedores/')
        .then(response => response.json())
        .then(data => setSellersInfo({ loading: false, items: data.vendedores}))
    }, []);

    useEffect(() => {
        if (email && sellersInfo.items) {
            if (sellersInfo.items.some(seller => seller.email.includes(email))) {
                setRepeatedEmail(true);
            } else {
                setRepeatedEmail(false);
            }
        }
    }, [email, sellersInfo.items])

    useEffect(() => {
        if(groupInfo.items && cnpj && !cnpj.includes('_')) {
            const tempArray = groupInfo.items.filter(element => SlugCNPJ(cnpj).includes(element.cnpj.toString()));
            if(tempArray.length > 0) {
                setGroup(tempArray[0].grupo);
                setInvalidCnpj(false);
            } else {
                setInvalidCnpj(true);
            }
        }
    }, [groupInfo, cnpj]);

    useEffect(() => {
        if(uf !== "Estado") {
            setCityList({ loading: true });
            fetch(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${uf}/municipios`)
            .then(response => response.json())
            .then(data => setCityList({ loading: false, items: data}))
        }
    }, [uf]);

    function CitySetter(name) {
        setCity(name);
        setShowCityList(false);
    }

    function RegisterSalesman() {
        const formData = {
            vendedore:
                {
                    nome: name,
                    cpf: cpf,                    
                    email: email,
                    telefone: phone,
                    cnpj: SlugCNPJ(cnpj),
                    cidade: city,  
                    estado: uf,    
                    grupo: group,                          
                    data: new Date(),  
                }
        }    
    
        fetch('https://api.sheety.co/0a34b7201289c69647d7f8c626404ba2/vendedoresCadastrados/vendedores/', {
            headers: { "Content-Type": "application/json; charset=utf-8" },
            method: 'POST',
            body: JSON.stringify(formData)
            }).then(response => {
                console.log(response)
                NotificationSetter();
                window.location.href =  "/"
            }).catch(error => {
                console.log(error.response);
        });
        

    }

    function SlugCNPJ(toSlug) {
        return toSlug.toString().replace(".","").replace(".","").replace("/","").replace("-","");
    }

    return (
        <div className="registration_form">
            <div className="form_group">
                <input type="text" id="inp_name" onChange={event => setName(event.currentTarget.value) } onFocus={() => setShowCityList(false)} value={name} placeholder="NOME" />
            </div>
            <div className="form_group">
                <label htmlFor="inp_cpf">CPF</label>
                <InputMask mask="999.999.999-99" placeholder="CPF" onChange={event => setCpf(event.currentTarget.value) } onFocus={() => setShowCityList(false)} value={cpf} />
            </div>
            <div className="form_group">
                <input type="text" id="inp_email" onChange={event => setEmail(event.currentTarget.value) } onFocus={() => setShowCityList(false)} value={email} placeholder="E-MAIL" />
            </div>
            <If test={repeatedEmail}>
                <div className="error">
                    <legend>E-mail Inválido</legend>
                </div>
            </If>  
            <div className="form_group">
                <InputMask mask="(99) 99999-9999" placeholder="TELEFONE" onChange={event => setPhone(event.currentTarget.value) } value={phone} />
            </div>
            <div className="form_group">
                <InputMask mask="99.999.999/9999-99" placeholder="CNPJ DA LOJA" onChange={event => setCnpj(event.currentTarget.value) } value={cnpj}/>
            </div>
            <If test={invalidCnpj}>
                <div className="error">
                    <legend>CNPJ Inválido</legend>
                </div>
            </If>            
            <div className="city_state">
                <div className="form_group state">
                    <select id="sel_uf" onChange={event => setUf(event.currentTarget.value) } value={uf} onFocus={() => setShowCityList(false)}>         
                        <option value="Estado" disabled hidden>ESTADO</option>       
                        <option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amapá</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Ceará</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espírito Santo</option>
                        <option value="GO">Goiás</option>
                        <option value="MA">Maranhão</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Pará</option>
                        <option value="PB">Paraíba</option>
                        <option value="PR">Paraná</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piauí</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>                
                        <option value="RO">Rondônia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">São Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantins</option>
                    </select>
                </div>
                <div className="form_group city_lister">
                    <input type="text" id="inp_city" onChange={event => setCity(event.currentTarget.value) } value={city} onFocus={() => setShowCityList(true)} disabled={uf === "Estado"} placeholder="CIDADE" />
                    <If test={cityList.items && showCityList}>
                    <AutocompleteCity CityList={cityList.items} CityName={city} CitySetter={CitySetter}/>
                </If>
                </div>
            </div>            
            <div className="nav_buttons">
                <Link to="/" className="router_link">Voltar</Link>
                <button disabled={!(name && cpf && cnpj && !cnpj.toString().includes('_') && phone && uf && city && !showCityList && !invalidCnpj && !repeatedEmail)} onClick={RegisterSalesman}>
                    Confirmar
                </button>      
            </div>         
        </div>
    )    
}

export default RegisterSeller;