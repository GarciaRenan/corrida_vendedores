import React, { useState, useEffect } from "react";

const  AutocompleteCity = (props) => {
  const [citiesToShow, setCitiesToShow] = useState(props.CityList);
  const cityList = props.CityList;
  const cityName = props.CityName;

  useEffect(() => {
    const tempArray = cityList.filter((city) =>
      Slugify(city.nome).includes(Slugify(cityName))
    );
    setCitiesToShow(tempArray.slice(0, 5));
  }, [cityName, cityList]);

  function Slugify(name) {
    const a = "àáâãçéêíôóú";
    const b = "aaaaceeioou";
    const p = new RegExp(a.split("").join("|"), "g");

    return name
      .toString()
      .toLowerCase()
      .replace(p, (c) => b.charAt(a.indexOf(c)));
  }

  return (
    <div className="AutocompleteCity">
      {citiesToShow.map((city) => {
        return (
          <button key={city.id} onClick={() => props.CitySetter(city.nome)}>
            {city.nome}
          </button>
        );
      })}
    </div>
  );
};

export default AutocompleteCity;