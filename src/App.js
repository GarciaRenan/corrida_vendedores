import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import If from "./components/If";
import moment from "moment";
import "moment/locale/pt-br";

import "./App.css";

export default function App() {
  const [saleData, setSaleData] = useState([
    { id: 0, quantidade: 0, referencia: 0, valor: 0 },
  ]);
  const [totalAmount, setTotalAmount] = useState(0);
  const [totalValue, setTotalValue] = useState(0);
  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);
  // const [leaderboard, setLeaderboard] = useState({loading: false, items: null });
  const [sellersList, setSellersList] = useState({
    loading: false,
    items: null,
  });
  const [sellerInfo, setSellerInfo] = useState(null);
  const [showTip, setShowTip] = useState(false);
  const [showNotification, setShowNotification] = useState(false);

  useEffect(() => {
    setSellersList({ loading: true });
    fetch(
      "https://api.sheety.co/b162774c8e40144ede1bd2961a7f282a/testeCorrida/vendedores"
    )
      .then((response) => response.json())
      .then((data) => {
        setSellersList({ loading: false, items: data["vendedores"] });
      });
  }, []);

  useEffect(() => {
    console.log(sellerInfo);
  }, [sellerInfo]);

  useEffect(() => {
    let tempAmount = 0;
    let tempValue = 0.0;

    saleData.forEach((sale) => {
      tempAmount += parseInt(sale.quantidade);
      tempValue += parseFloat(sale.valor) * parseFloat(sale.quantidade);
    });

    setTotalAmount(tempAmount);
    setTotalValue(tempValue);
  }, [saleData]);

  useEffect(() => {
    if (email) {
      const tempArray = sellersList.items.filter(
        (element) => element.email === email
      );
      const hasEmail = (element) => element.email === email;
      setValidEmail(sellersList.items.some(hasEmail));
      setSellerInfo(tempArray[0]);
    }
  }, [email, sellersList]);

  function ChangeSaleData(index, key, value) {
    const tempArray = saleData.map((sale) =>
      sale.id === index ? { ...sale, [key]: value } : sale
    );
    setSaleData(tempArray);
  }

  function AddProduct() {
    const newLine = {
      id: saleData[saleData.length - 1].id + 1,
      quantidade: 0,
      referencia: 0,
      valor: 0,
    };
    const tempArray = saleData.map((sale) => sale);
    tempArray.push(newLine);
    setSaleData(tempArray);
  }

  function RemoveProduct(index) {
    const tempArray = saleData.filter((sale) => sale.id !== index);
    setSaleData(tempArray);
  }

  function PostToForm() {
    const formData = {
      venda: {
        nome: sellerInfo.nome,
        whatsapp: sellerInfo.whatsapp,
        cidade: sellerInfo.cidade,
        uf: sellerInfo.uf,
        instagramPessoal: sellerInfo.instagramPessoal,
        instagramLoja: sellerInfo.instagramLoja,
        email: email,
        quantidadeTotal: totalAmount,
        valorTotal: totalValue,
        detalhes: JSON.stringify(saleData),
        data: moment(new Date()).format("DD/MM/YYYY HH:mm:ss"),
      },
    };

    fetch(
      "https://api.sheety.co/b162774c8e40144ede1bd2961a7f282a/testeCorrida/vendas",
      {
        headers: { "Content-Type": "application/json; charset=utf-8" },
        method: "POST",
        body: JSON.stringify(formData),
      }
    )
      .then(NotificationSetter())
      .then((response) => {
        setTimeout(() => this.setState({ navigate: true }), 2000);
        window.location.href = "/";
      })
      .catch((error) => {
        console.log(error.response);
      })
      .then
      // GetLeaderBoard()
      ();
  }

  function NotificationSetter() {
    setShowNotification(true);
  }

  return (
    <div className="App">
      <div className="gradient_overlay"></div>
      <Router>
        <Switch>
          <Route exact path="/">
            <div className="stage">
              <div className="div-center">
                <If test={showNotification}>
                  <div className="notification">
                    Cadastro realizado com sucesso!
                  </div>
                </If>
              </div>
              <div className="sales_table">
                <div className="table_header">
                  <legend>Quantidade</legend>
                  <legend>Referência</legend>
                  <legend>Valor Unitário (R$)</legend>
                </div>
                {saleData.map((sale) => {
                  return (
                    <div className="sale_row" key={sale.id}>
                      <input
                        type="number"
                        value={sale.quantidade}
                        onChange={(event) =>
                          ChangeSaleData(
                            sale.id,
                            "quantidade",
                            event.currentTarget.value
                          )
                        }
                      />
                      <input
                        type="number"
                        value={sale.referencia}
                        onChange={(event) =>
                          ChangeSaleData(
                            sale.id,
                            "referencia",
                            event.currentTarget.value
                          )
                        }
                      />
                      <input
                        type="number"
                        value={sale.valor}
                        onChange={(event) =>
                          ChangeSaleData(
                            sale.id,
                            "valor",
                            event.currentTarget.value
                          )
                        }
                        step="0.01"
                      />
                      <If test={sale.id !== 0}>
                        <button onClick={() => RemoveProduct(sale.id)}>
                          -
                        </button>
                      </If>
                    </div>
                  );
                })}
              </div>

              <button onClick={AddProduct} className="btn_add_product">
                Adicionar um produto
              </button>

              <div className="input_group">
                <label htmlFor="inp_email">
                  Escreva seu e-mail para cadastrar as vendas
                </label>
                <input
                  type="email"
                  id="inp_email"
                  value={email}
                  onChange={(event) => setEmail(event.currentTarget.value)}
                  onFocus={() => setShowTip(true)}
                  onBlur={() => setShowTip(false)}
                />
                <If test={email.length > 5 && !validEmail && showTip}>
                  <legend>
                    Após preencher este campo, se o botão "CADASTRAR" não
                    aparecer, seu e-mail ainda não está cadastrado
                  </legend>
                </If>
              </div>

              <div className="nav_buttons">
                <If test={validEmail && totalAmount > 0 && totalValue > 0}>
                  <button onClick={PostToForm}>Cadastrar</button>
                </If>
              </div>
            </div>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}
